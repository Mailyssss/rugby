from django.urls import path
# Import des URLs de l'interface d'administration
from django.contrib import admin
# Import des vues qui sont déclarées dans leur propre module (dossier)
from .views import HomeView, StadiumsView, TeamsView, NewsletterView, UpdateView,AboutView, RecupView

urlpatterns = (
    path("", HomeView.as_view(), name="home"),
    path("stadiums", StadiumsView.as_view(), name="stadiums"),
    path("teams", TeamsView.as_view(), name="teams"),
    path("newsletter", NewsletterView.as_view(), name="newsletter"),
    path("about", AboutView.as_view(), name= "about"),
    path("update", UpdateView.as_view(), name="update"),
    # Dans un cadre de projet réel, il serait préférable d'utiliser une URL moins prévisible que "admin"
    path("admin", admin.site.urls),
    path("api/stadium/", RecupView.getStadium, name = "stad"),
    path("api/team/", RecupView.getTeam, name = "team"),
    path("api/event/", RecupView.getEvent, name = "event"),
    path("api/ticket/<str:id>", RecupView.getTicket, name = "ticket")
)

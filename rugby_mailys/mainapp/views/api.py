from mainapp.models import Event,Stadium,Team,Ticket
from django.http import JsonResponse
from django.views import View

class RecupView(View): 
    def getStadium(request):
        stad = Stadium.objects.all().values()
        return JsonResponse(list(stad), safe=False)

    def getTeam(request):
        team = Team.objects.all().values()
        return JsonResponse(list(team), safe=False)

    def getEvent(request):
        event = Event.objects.all().values()
        return JsonResponse(list(event), safe=False)
        
    def getTicket(request,id):
        ticket = Ticket.objects.filter(id=id).values()
        return JsonResponse(list(ticket), safe=False)


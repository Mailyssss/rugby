async function recolte_donnee() {
  const reponse_stadium = await fetch("http://127.0.0.1:8000/api/stadium");
  const stadium = await reponse_stadium.json();

  const reponse_event = await fetch("http://127.0.0.1:8000/api/event");
  const event = await reponse_event.json();

  const reponse_team = await fetch("http://127.0.0.1:8000/api/team");
  const team = await reponse_team.json();

  for (var i = 0; i < event.length; i++) {
    const node = document.getElementById("match");
    node.style.display = "block";
    const clone = node.cloneNode(true);
    node.style.display = "none";

    // --------------------Event--------------------------------------------

    const eventElement = document.createElement("div");
    eventElement.id = "eventIdDiv";
    eventElement.innerHTML =  (event[i]["id"]);
    clone.appendChild(eventElement);

    // --------------------Stadium---------------------------------------------

    const stadiumId = event[i]["stadium_id"] - 1;
    const stadiumElement = document.createElement("div");
    stadiumElement.id = "stadiumElemDiv";
    stadiumElement.innerHTML =  (stadium[stadiumId]["name"]);
    clone.appendChild(stadiumElement);

    // --------------------TEAM---------------------------------------------
    // Team div
    const teamElement = document.createElement("div");
    teamElement.id = "teamElemDiv";

    // VS div
    const vsElement = document.createElement("div");
    vsElement.id = "vsElemDiv";
    const vsText = document.createTextNode("VS");
    vsElement.appendChild(vsText);

    // Div team 1 + img
    const teamFlag = document.createElement("div");
    teamFlag.id = "teamFlag";

    // Div team 2 + img
    const teamDeuxFlag = document.createElement("div");
    teamDeuxFlag.id = "teamDeuxFlag";

    // Team 1 div
    const teamUnElement = document.createElement("div");
    teamUnElement.id = "teamUnElemDiv";

    // Team 2 div
    const teamDeuxElement = document.createElement("div");
    teamDeuxElement.id = "teamDeuxElemDiv";

    // Drapeau img
    const drapElem = document.createElement("img");
    drapElem.id = "drapeau";
    const drapDeuxElem = document.createElement("img");
    drapDeuxElem.id = "drapeau";

    //-----------Team 1 -----------------
    if (event[i]["team_home_id"] == null) {
      teamUnElement.innerHTML = "undefined";
      teamFlag.appendChild(teamUnElement);

      drapElem.setAttribute("src", "flags/comingsoon.svg");
      drapElem.setAttribute("width", "30");
      teamFlag.appendChild(drapElem);
      teamElement.appendChild(teamFlag);
    } else {
      const teamDomicile = event[i]["team_home_id"] - 1;
      teamUnElement.innerHTML =  (team[teamDomicile]["nickname"]);
      teamFlag.appendChild(teamUnElement);

      const drapeauTag = team[teamDomicile]["country_alpha2"];
      drapElem.setAttribute("src", "flags/" + drapeauTag + ".svg");
      drapElem.setAttribute("width", "30");
      teamFlag.appendChild(drapElem);
      teamElement.appendChild(teamFlag);
    }

    //-----------VS-----------------
    teamElement.appendChild(vsElement);

    //-----------Team 2 -----------------
    if (event[i]["team_away_id"] == null) {
      teamDeuxElement.innerHTML = "undefined";
      teamDeuxFlag.appendChild(teamDeuxElement);

      drapDeuxElem.setAttribute("src", "flags/comingsoon.svg");
      drapDeuxElem.setAttribute("width", "30");

      teamDeuxFlag.appendChild(drapDeuxElem);
      teamElement.appendChild(teamDeuxFlag);
    } else {
      const teamExterne = event[i]["team_away_id"] - 1;

      teamDeuxElement.innerHTML =  (team[teamExterne]["nickname"]);
      teamDeuxFlag.appendChild(teamDeuxElement);

      const drapeauDeuxTag = team[teamExterne]["country_alpha2"];
      drapDeuxElem.setAttribute("src", "flags/" + drapeauDeuxTag + ".svg");
      drapDeuxElem.setAttribute("width", "30");
      teamDeuxFlag.appendChild(drapDeuxElem);
      teamElement.appendChild(teamDeuxFlag);
    }
    
    clone.appendChild(teamElement);

    // date

    const startHourElement = document.createElement("div");
    startHourElement.id = "startHourElemDiv";
    var date = new Date(event[i]["start"]);
    startHourElement.innerHTML = date.toString('YYYY-MM-dd');
    clone.appendChild(startHourElement);

    //------------match--------------------------------------
    document.getElementById("matchs").appendChild(clone);
  }
}

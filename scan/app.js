
async function scanQR(){
    let input = document.getElementById("fileTicket");
    let fileTicket = input.files[0];

    if(fileTicket   ) {
        QrScanner.scanImage(fileTicket)
        .then(async result => {
            // alert(result);
            

            const reponse_qr = await fetch("http://127.0.0.1:8000/api/ticket/"+result)
            const resultQR = await reponse_qr.json();
            var resultQRleVrai = resultQR[0]

            const reponse_stadium = await fetch("http://127.0.0.1:8000/api/stadium");
            const stadium = await reponse_stadium.json();
          
            const reponse_event = await fetch("http://127.0.0.1:8000/api/event");
            const event = await reponse_event.json();
          
            const reponse_team = await fetch("http://127.0.0.1:8000/api/team");
            const team = await reponse_team.json();

            // Ticket affiché

            const nodeTicket = document.getElementById("ticketAffiche");
            const cloneTicket = nodeTicket.cloneNode(true);
            nodeTicket.style.display = "none"

                // Team div
    const teamElement = document.createElement("div");
    teamElement.id = "teamElemDiv";


                // Div team 1 + img
    const teamFlag = document.createElement("div");
    teamFlag.id = "teamFlag";

    // Div team 2 + img
    const teamDeuxFlag = document.createElement("div");
    teamDeuxFlag.id = "teamDeuxFlag";

    // Team 1 div
    const teamUnElement = document.createElement("div");
    teamUnElement.id = "teamUnElemDiv";

    // Team 2 div
    const teamDeuxElement = document.createElement("div");
    teamDeuxElement.id = "teamDeuxElemDiv";

    // Drapeau img
    const drapElem = document.createElement("img");
    const drapDeuxElem = document.createElement("img");

            //stadium name

            const stadiumId = event[resultQRleVrai["event_id"]]["stadium_id"] - 1;
            const stadiumElement = document.createElement("div");
            stadiumElement.id = "stadiumElemDiv";
            stadiumElement.innerHTML =  (stadium[stadiumId]["name"]);
            cloneTicket.appendChild(stadiumElement);

            //stadium location

            const stadiumLocationElement = document.createElement("div");
            stadiumLocationElement.id = "stadiumLocationElemDiv";
            stadiumLocationElement.innerHTML =  (stadium[stadiumId]["location"]);
            cloneTicket.appendChild(stadiumLocationElement);

            //team home

    if (event[resultQRleVrai["event_id"]]["team_home_id"] == null) {
        teamUnElement.innerHTML = "undefined";
        teamFlag.appendChild(teamUnElement);
  
        drapElem.setAttribute("src", "flags/comingsoon.svg");
        drapElem.setAttribute("width", "30");
        teamFlag.appendChild(drapElem);
        teamElement.appendChild(teamFlag);
      } else {
        const teamDomicile = event[resultQRleVrai["event_id"]]["team_home_id"] - 1;
        teamUnElement.innerHTML =  (team[teamDomicile]["nickname"]);
        teamFlag.appendChild(teamUnElement);
  
        const drapeauTag = team[teamDomicile]["country_alpha2"];
        drapElem.setAttribute("src", "flags/" + drapeauTag + ".svg");
        drapElem.setAttribute("width", "30");
        teamFlag.appendChild(drapElem);
        teamElement.appendChild(teamFlag);
      }

            //team externe

            if (event[resultQRleVrai["event_id"]]["team_away_id"] == null) {
                teamDeuxElement.innerHTML = "undefined";
                teamDeuxFlag.appendChild(teamDeuxElement);
          
                drapDeuxElem.setAttribute("src", "flags/comingsoon.svg");
                drapDeuxElem.setAttribute("width", "30");
          
                teamDeuxFlag.appendChild(drapDeuxElem);
                teamElement.appendChild(teamDeuxFlag);
              } else {
                const teamExterne = event[resultQRleVrai["event_id"]]["team_away_id"] - 1;
          
                teamDeuxElement.innerHTML =  (team[teamExterne]["nickname"]);
                teamDeuxFlag.appendChild(teamDeuxElement);
          
                const drapeauDeuxTag = team[teamExterne]["country_alpha2"];
                drapDeuxElem.setAttribute("src", "flags/" + drapeauDeuxTag + ".svg");
                drapDeuxElem.setAttribute("width", "30");
                teamDeuxFlag.appendChild(drapDeuxElem);
                teamElement.appendChild(teamDeuxFlag);
              }
              
              cloneTicket.appendChild(teamElement);

            // date

            const startHourElement = document.createElement("div");
            startHourElement.id = "startHourElemDiv";
            // startHourElement.innerHTML =  (event[resultQRleVrai["event_id"]]["start"]);
            var date = new Date(event[resultQRleVrai["event_id"]]["start"]);
            startHourElement.innerHTML = date.toString('YYYY-MM-dd');
            cloneTicket.appendChild(startHourElement);

            
            //categorie

            const categorieElem = document.createElement("div");
            categorieElem.id = "categorieElem";
            categorieElem.innerHTML =  "Catégorie : " + (resultQRleVrai["category"]);
            cloneTicket.appendChild(categorieElem)

            //seat

            const seatElem = document.createElement("div");
            seatElem.id = "seatElem";
            seatElem.innerHTML =  "Siège : " + (resultQRleVrai["seat"]);
            cloneTicket.appendChild(seatElem)

            //prix
            
            const priceElem = document.createElement("div");
            priceElem.id = "seatElem";
            priceElem.innerHTML = "Price : " + (resultQRleVrai["price"]) + (resultQRleVrai["currency"]);
            cloneTicket.appendChild(priceElem)

            document.getElementById("tickets").appendChild(cloneTicket);
        })
    }
}


// const startScanner = () => {
//     const videoElem = document.querySelector("#scanner video");

//     const qrScanner = new QrScanner(videoElem, (result) => {
//         console.log('Contenu du QR Code :', result);
//     });
    
//     qrScanner.setCamera("environment");
//     qrScanner.start();
// }

// document.querySelector("#start").addEventListener("click", () => {
//     startScanner();
// });
